#Variable a inicializada en None
a = None
#se asigna a la variable a un valor de tipo string
a = str(input ("Ingrese la palabra Hola:"))
#se condiciona la variable para entregar un mensaje
if (a == 'Hola'):
    print("Chao")
else:
    print("No entiendo tu mensaje ", a)
