#Se imprime un aviso para el cliente
print("Oferta válida por la compra desde 3 productos")
#A la variable a se le asignara el valor (de tipo entero) que ingrese el cliente
a = int(input("Ingrese cuantos trajes va a llevar:"))
d = 0
g = 0
#En este ciclo se utiliza para condicionar la variable a, puesto que si se lleva menos de 3 trajes
#arroje un mensaje, y luego vuelva a pedir que ingrese un valor para asignarselo a la variable a
while (a < 3):
    print("Dato ingresado invalido, por favor intentelo de nuevo")
    a = int(input("Ingrese cuantos trajes va a llevar:"))
#El ciclo for se utilizo para ejecutar la asignacion de un valor a la varibale nuevoDato 
#una cantidad de veces determinada por la variable a+1
for i in range(1, a+1):
    nuevoDato = int(input("Ingrese el precio del traje {}: ". format(i)))
#Se condiciona la varibale dato para determinar que procedimiento llevar a cabo
#y asi entragr los datos pertinentes al usuario
    if (nuevoDato > 10000):
        b = nuevoDato*(15/100)
        c = nuevoDato - b
        d = d + c
        print("Se le ha aplicado un descuento de: $", b)
        print("El precio del traje quedo en: $", c)
    elif (nuevoDato < 10000):
        e = nuevoDato*(8/100)
        f = nuevoDato - e
        g = g + f
        print("Se le ha aplicado un descuento de: $",e)
        print("El precio del traje quedo en: $", f)
h = g + d
print("El total a pagar con los descuentos incluidos es de: $",h)

