#Se declara una variable a la cual se le asigna el valor de el numero (de tipo entero)
#de lapices que la persona va a comprar
a = int(input("Ingrese la cantidad de lapices que desea comprar: "))
#Se condiciona para decidir cuanto se cobrara segun el numero de lapices que se compre
#La condicion que se cumpla, hará su respectiva operacion guardandose en la variable declarada
#y luego se imprimira el valor de la variable
if (a >= 1000):
    b = a*85
    print("Usted debe pagar: $",b)
elif (a < 1000):
    c = a*90
    print("Usted debe pagar: $",c)
