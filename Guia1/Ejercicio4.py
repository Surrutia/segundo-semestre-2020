#Se le asigna a la variable a un valor de tipo entero
a = int(input("Ingrese el numero de alumnos:"))
c = 0
#El ciclo condiciona la variable a para que el usuario asigne un valor distinto de 0
while (a == 0):
    print("Por favor ingrese un numero de alumnos distinto a 0")
    a = int(input("Ingrese el numero de alumnos:"))
#El ciclo for ejecutara la asignacion del valor de b, segun el valor de a+1
for i in range (1, a+1):
    b = int(input("Ingrese la edad del alumno {}:". format(i)))
    c = c + b
d = c/a
print("La edad promedio es:", d)
