
def introducir(traductor):
    palabra, traduccion = input("Ingrese palabra separada por"
                                " un punto y coma (;): ").split(";")
    traductor.update({palabra: traduccion})
    print(traductor)
    return traductor

def menu(traductor):
    intro = input("¿Desea seguir introduciendo palabras? S/N: ")
    if intro.upper() == "S":
        introducir(traductor)
        menu(traductor)
    elif intro.upper() == "N":
        frase = input("Ingrese la frase que desea traducir: ")
        for frase in traductor:
            print(traductor[frase])
    return traductor
traductor = {}
introducir(traductor)
menu(traductor)
